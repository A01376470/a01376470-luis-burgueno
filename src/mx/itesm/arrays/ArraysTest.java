/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.arrays;

import mx.itesm.actividad06.Author;

/**
 *
 * @author luis_
 */
public class ArraysTest {
    
    public static void main(String[] args) {
        
        Author[] authors = new Author[5];
        authors[0] = new Author("Felipe", "a@a.mx", 'm');
        authors[1] = new Author("Lucia", "b@b.mx", 'f');
        authors[2] = new Author("Jose", "c@c.mx", 'u');
        authors[3] = new Author("Maria", "d@d.mx", 'u');
        authors[4] = new Author("Ivan", "e@e.mx", 'u');
        Arrays a = new Arrays();
        Arrays.printArray(authors);
        // double [] nums = new double[5];
        double[] suma = {1, 2, 3, 4};
        System.out.println(Arrays.sumArray(suma));
        System.out.println(a.averageArray(suma));
        int[] nums = {1, 2, 3, 4};
        System.out.println(a.indexOf(nums, 5));
        
        Author[] autores = new Author[55];
        int numA = 0;
        for (Author autor:autores){
                numA++;
        }
        System.out.println(numA);
        
    }
    
}
