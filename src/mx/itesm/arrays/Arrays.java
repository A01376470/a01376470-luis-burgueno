/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.arrays;

/**
 *
 * @author luis_
 */
public class Arrays {
    
    public static void printArray(Object[] arr) {
        if (arr.length == 0) {
            System.out.println("[]");
            return;
        }
        
        String result = "[" + arr[0];
        for(int i =1; i<arr.length;i++){
            result += ", " + arr[i];
        }
        result += "]";
        System.out.println(result);
    }
    
    public static double sumArray(double[] arr){
        double suma= 0;
        for (double element: arr){
            suma += element;
        }
        return suma;
    }
    
    public double averageArray(double[] arr) {
        if (arr.length == 0) {
            return 0;
        }
        double suma= sumArray(arr);
        return suma / arr.length;
    }
    
    public int indexOf(int[] arr, int e) {
        for (int i = 1; i<arr.length;i++){
            if (arr[i] == e){
                return i;
            }
        }
        return -1;
    }
}
