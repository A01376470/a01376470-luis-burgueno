/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia01;

/**
 *
 * @author luis_
 */
public class Gerente extends Empleado{
    
    private int empleados;
    
    public Gerente (String nombre, float sueldo, String nomina) {
        super(nombre, sueldo, nomina); // Mandar llamar a la super clase/clase padre
        
        empleados = 0;
    }
    
    public void agregarEmpleado() {
        empleados++;
    }
    
    @Override
    public String toString() {
        return "Gerente con nómina " + nomina + ": " + nombre + ". Tiene " + empleados + " empleados a su cargo";
        
    }
}
