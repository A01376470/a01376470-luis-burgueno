/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia01;

/**
 *
 * @author luis_
 */
public class Empleado {
    protected String nombre;
    protected float sueldo;
    protected String nomina; // Para que lo pueda ver la clase y sus clases hijas.
    
    public Empleado(String nombre, float sueldo, String nomina) {
        this.nombre = nombre;
        this.sueldo = sueldo;
        this.nomina = nomina;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public float getSueldo(){
        return sueldo;
    }
    
    public String getNomina() {
        return nomina;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }
    
    public void setNomina(String nomina) {
        this.nomina = nomina;
    }
    
    @Override // cuando estoy sobreescribiendo el método.
    public String toString() {
        return "Empleado con nomina: " + nomina + ": " + nombre;
    }
    
    @Override
    public boolean equals(Object other) {
        
        Empleado otherEmployee = (Empleado)other;
        
        if(nombre.equals(otherEmployee.nombre) && sueldo == otherEmployee.sueldo && nomina.equals(otherEmployee.nomina)) {
            return true;
        }
        
        return false;
    }
}

