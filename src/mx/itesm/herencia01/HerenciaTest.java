/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia01;

/**
 *
 * @author luis_
 */
public class HerenciaTest {
    
    public static void main(String[] args) {
        Empleado oriam = new Empleado("Oriam", 10.0f, "L01234567");
        System.out.println(oriam);
        
        /*
        No podemos hacer un down cast a gerente porque oriam es un objeto de tipo Empleado
        Gerente oriamGerente = (Gerente)oriam;
        oriamGerente.agregarEmpleado();
        System.out.println(oriamGerente);
        */
        
        Gerente paty = new Gerente("Paty", 20.0f, "L00123456");
        paty.setSueldo(30.0f);
        System.out.println(paty);
        
        paty.agregarEmpleado();
        System.out.println(paty);
        
        // Ejemplo de polimorfismo, se llama como empleado pero se crea como gerente.
        // La variable es tipo empleado, aunque el objeto sea de tipo gerente.
        // No dejaría usar, por ejemplo, diego.agregarSubordinado()
        // No se puede hacer al revés, no es bilateral.
        // Estamos haciendo un up cast implícito; gerente -> empleado
        Empleado diego = new Gerente("Diego", 15.0f, "L00012345");
        System.out.println(diego.toString());
        
        Gerente diegoGerente = (Gerente)diego; // down cast, tiene que ser explícito
        diegoGerente.agregarEmpleado();
        System.out.println(diegoGerente.toString());
        System.out.println(diego);
        
        Empleado[] empleadosArreglo = new Empleado[5];
        empleadosArreglo[0] = new Empleado("Jorge", 50.0f, "L01");
        empleadosArreglo[1] = new Empleado("Oscar", 10.0f, "L02");
        empleadosArreglo[2] = new Gerente("Betsy", 100.0f, "L03");
        empleadosArreglo[3] = new Gerente("Susy", 120.0f, "L04");
        empleadosArreglo[4] = new Empleado("Julio", 200.0f, "L05");
        
        for(int i = 0; i < empleadosArreglo.length; i++) {
            System.out.println(empleadosArreglo[i].toString());
        }
    }
}
