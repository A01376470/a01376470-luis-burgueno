/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad05;

/**
 *
 * @author luis_
 */
public class TareaTest {
    
    public static void main(String[] args) {
        Shape[] shapes = new Shape[9];
        shapes[1] = new Circle();
        shapes[2] = new Circle(3.0);
        shapes[3] = new Circle(2.0, "red", true);
        shapes[4] = new Rectangle();
        shapes[5] = new Rectangle(3.0, 4.0);
        shapes[6] = new Rectangle(2.0, 4.0, "green", true);
        shapes[7] = new Square();
        shapes[8] = new Square(2.0);
        shapes[0] = new Square(5.0, "blue", true);
        
        Circle c = (Circle) shapes[2];
        Rectangle r = (Rectangle) shapes[5];
        Square s = (Square) shapes[8];
        
        System.out.println(shapes[1].getArea());
        System.out.println(shapes[4].getArea());
        System.out.println(shapes[7].getArea());
        System.out.println(c.getRadius());
        System.out.println(r.getLength());
        System.out.println(r.getWidth());
        System.out.println(s.getSide());
        
        c.setColor("gold");
        c.setFilled(true);
        c.setRadius(5.0);
        System.out.println(c);
        System.out.println(c.getArea());
        System.out.println(c.getPerimeter());
        
        r.setColor("silver");
        r.setFilled(true);
        r.setLength(3.0);
        r.setWidth(8.0);
        System.out.println(r);
        System.out.println(r.getArea());
        System.out.println(r.getPerimeter());
        
        s.setColor("bronze");
        s.setFilled(true);
        s.setLength(3.0);
        System.out.println(s);
        s.setWidth(8.0);
        System.out.println(s);
        s.setSide(2.0);
        System.out.println(s);
        System.out.println(s.getArea());
        System.out.println(s.getPerimeter());
        
    }
}
