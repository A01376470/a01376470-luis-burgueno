/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad05;

/**
 *
 * @author luis_
 */
public class Square extends Rectangle{
    
    public Square() {
        super();
    }
    
    public Square(double side) {
        super();
        this.width = side;
    }
    
    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);    
    }
    
    public double getSide(){
        return width;
    }
    
    public void setSide(double side) {
        this.width = side;
        this.length = side;
    }
    
    @Override
    public void setWidth(double side) {
        this.width = side;
        this.length = side;
    }
    
    @Override
    public void setLength(double side) {
        this.length = side;
        this.width = side;
    }
    
    @Override
    public String toString() {
        return "Square [side = " + width + ", color = " + color + ",filled = " + filled + "]";
    }
    
    
    
    
}
