/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad03;

/**
 *
 * @author luis_
 */
public class Cylinder extends Circle {
    private double height;
    
    public Cylinder(){
        super();
        height = 1.0d;
    }
    
    public Cylinder(double radius) {
        super(radius);
        height = 1.0d;
    }
    
    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }
    
    public Cylinder(double radius, double height, String color) {
        super(radius, color);
        this.height = height;
    }
    
    public double getHeight() {
        return height;
    }
    
    public void setHeight(double height) {
        this.height = height;
    }
    
    @Override
    public String toString() {
        return "Cylinder [radius = " + super.getRadius() + ", height = " + height + ", color = " + super.getColor() + "]";
    }
    
    @Override
    public double getArea() { 
        double area = (2 * Math.PI * super.getRadius() * height) + (2 * super.getArea());
        return area;
    }
    
    public double getVolume() {
        double volume = super.getArea() * height;
        return volume;
    }
}
