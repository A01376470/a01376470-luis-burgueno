/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad03;

/**
 *
 * @author luis_
 */
public class testTarea {
    
    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(2.0);
        Circle c3 = new Circle(3.0, "blue");
        
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        
        System.out.println(c1.getArea());
        System.out.println(c2.getArea());
        System.out.println(c3.getArea());
        
        c1.setRadius(2.0);
        c2.setRadius(4.0);
        c3.setColor("green");
        
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        
        Cylinder c4 = new Cylinder();
        Circle c5 = new Cylinder(2);
        Circle c6 = new Cylinder(2, 2);
        Circle c7 = new Cylinder(2, 2, "violet");
        
        System.out.println(c4);
        System.out.println(c5);
        System.out.println(c6);
        System.out.println(c7);
        
        c4.setHeight(2);
        System.out.println(c4.getArea());
        System.out.println(c4.getVolume());
        
        
        
    }
}
