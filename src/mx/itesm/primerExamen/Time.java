/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.primerExamen;

/**
 *
 * @author luis_
 */
public class Time {
    private int hour;
    private int minute;
    private int second;
    
    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this. second = second;
    }
    
    public int getHour() {
        return hour;
    }
    
    public int getMinute() {
        return minute;
    }
    
    public int getSecond() {
        return second;
    }
    
    public void setHour(int hour){
        if (hour < 0 || hour >= 24){
            hour = 0;
        }
        this.hour = hour;
    }
    
    public void setMinute(int minute){
        if (minute <0 || minute >= 60){
            minute = 0;
        }
        this.minute = minute;
    }
    
    public void setSecond(int second){
        if (second < 0 || second >= 60){
            second = 0;
        }
        this.second = second;
    }
    
    public void setTime(int hour, int minute, int second){
        setHour(hour);
        setMinute(minute);
        setSecond(second);
    }
    
    public String toString() {
        String hourString = "";
        if (hour <= 9) {
            hourString = "0" + hour;
        } else {
            hourString = hourString + hour;
        }
        
        String minuteString = "";
        if(minute <= 9) {
            minuteString = "0" + minute;
        } else {
            minuteString += minute;
        }
        
        String secondString = "";
        if(second<=9) {
            secondString = "0"+ second;
        } else{
            secondString = "" + second;
        }
        
        return hourString + ":" + minuteString + ":" + secondString;
    }
    
    public void nextSecond() {
        second += 1;
        
        if (second > 59){
            second = 0;
            minute ++;
        
        
            if (minute > 59) {
                minute = 0;
                hour ++;
            
        
                if (hour > 23) {
                    hour = 0;
                }
            }
        }
    }
}
