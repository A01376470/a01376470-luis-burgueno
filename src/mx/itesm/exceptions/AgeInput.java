/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author luis_
 */
public class AgeInput {
    
    private static final String DEFAULT_MESSAGE = "Your age: ";
    private static final int DEFAULT_LOWER_BOUND = 0;
    private static final int DEFAULT_UPPER_BOUND = 99;
    
    private Scanner scanner;
    private int minAge;
    private int maxAge;
    
    public AgeInput(){
        this(DEFAULT_LOWER_BOUND, DEFAULT_UPPER_BOUND);
    }
    
    public AgeInput(int minAge, int maxAge){
        if(minAge>maxAge){
            throw new IllegalArgumentException("Max age should be greater than min age");
        }
        
        scanner = new Scanner(System.in);
        this.minAge = minAge;
        this.maxAge = maxAge;
    }
    
    public int getAge()throws Exception {
        return getAge(DEFAULT_MESSAGE);
    }
    
    public int getAge(String prompt) throws Exception{
        
        while(true) {
            System.out.print(prompt);
            int age = 0; 

            try {
                age = scanner.nextInt();
                
                if(age< minAge || age>maxAge){
                    throw new AgeInputException("Input out of bounds", minAge, maxAge, age);
                }
                return age;
            } catch(InputMismatchException e) {
                scanner.next();
                System.out.println("Invalid entry. Please enter digits only");
            }  finally {
                System.out.println("DONE");
            }
        }
    }
    
}
