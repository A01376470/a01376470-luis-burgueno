/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.exceptions;

import java.util.Scanner;
import java.util.GregorianCalendar;
import java.util.Calendar;

/**
 *
 * @author luis_
 */
public class ExceptionsTest {

    public static void main(String args[]){
        AgeInput input = new AgeInput(10, 50);
        int age = 0; 
        try{
            age =input.getAge("How old are you? ");
        } catch(Exception e){
            System.out.println(e.getMessage());
            System.out.println("Something happened, try another day.");
            return;
        }
        GregorianCalendar today = new GregorianCalendar();
        int thisYear = today.get(Calendar.YEAR);
        int bornYear = thisYear-age;
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Already had your birthday this year? (Y or N)");
        String answer = scanner.next();
        
        if (answer.equals("N")||answer.equals("n")){
            bornYear--;
        }
        
        System.out.println("\nYou were born in " + bornYear);
    }
    
}
