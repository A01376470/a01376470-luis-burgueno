/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad07;



/**
 *
 * @author luis_
 */
public class Actividad07Test {
    
    public static void main(String[] args) {
        Author a = new Author("Sanderson", "qweqwe@asd.com", 'm');
        Author b = new Author("Tolkien", "asd@gmail.com", 'm');
        Author c = new Author("De la Cruz", "sor@yahoo.com", 'f');
        Author[] as = new Author[3];
        as[0] = a;
        as[1] = b;
        as[2] = c;
        Book book = new Book("El Camino de los Reyes", as, 230.0);
        Book libro = new Book("Nacidos de la Bruma", as, 200.0, 20);
        
        System.out.println(book);
        System.out.println(libro);
        System.out.println(libro.getAuthors());
        System.out.println(libro.getAuthorNames());
        book.setPrice(300);
        book.setQty(40);
        System.out.println(book.getName());
        System.out.println(book.getPrice());
        System.out.println(book.getQty());
        System.out.println(book);
        book.addPage("bla");
        System.out.println(book.getPage(0).getContents());
        book.setPage("blabla", 0);
        System.out.println(book.getPage(0).getContents());
        book.addPage("bl");
        book.addPage("ba");
        book.addPage("la");
        book.addPage("a");
        System.out.println(book.getPage(4).getContents());
        book.addPage("ala");
        book.addPage("bla");
        book.getPage(4).setContents("hola");
        System.out.println(book.getPage(4).getContents());
    }
}
