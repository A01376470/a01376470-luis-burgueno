/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad07;

/**
 *
 * @author luis_
 */
public class Page {
    private String contents;
    
    public Page(String contents) {
        this.contents = contents;
    }
    
    public String getContents() {
        return contents;
    }
    
    public void setContents(String contents){
        this.contents = contents;
    }
}
