/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia02;

/**
 *
 * @author luis_
 */
public class Perro extends Mascota{
    
    private int breedType = 0;
    
    public int getBreedType() {
        return breedType;
    }
    
    public void setBreedType(int breedType) {
        this.breedType = breedType;
    }
    
    @Override
    public void sonido() {
        System.out.println("Woof");
    }
    
    @Override
    public void jugar() {
        System.out.println("Persigue su cola");
    }
    
    @Override
    public boolean equals(Object other) {
        
        /*if (other instanceof Perro) {
            Perro otherDog = (Perro)other;

            if ( otherDog.breedType == breedType) {
                return true;
            }
        }
        
        return false;*/
        
        if (other.getClass() == getClass()) {
            Perro otherDog = (Perro)other;
            
            return otherDog.breedType == breedType;
        }
        return false;
    }
}
