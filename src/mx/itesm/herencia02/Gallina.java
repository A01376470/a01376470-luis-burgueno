/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia02;

/**
 *
 * @author luis_
 */
public class Gallina extends AnimalGranja {
    
    @Override
    public void sonido() {
        System.out.println("Coooo Coooo");
    }
    
    @Override
    public boolean produceLeche() {
        return false;
    }
}
