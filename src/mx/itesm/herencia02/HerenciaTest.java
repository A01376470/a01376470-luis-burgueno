/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia02;

/**
 *
 * @author luis_
 */
public class HerenciaTest {
    
    public static void main(String[] args) {
        /*// no puedes instancear una clase abstracta
        Animal[] animals = new Animal[5];
        animals[0] = new Vaca();
        animals[1] = new Gallina();
        animals[2] = new Perro();
        animals[3] = new Gato();
        animals[4] = new Gato();
        
        for(Animal a : animals) {
            a.sonido();
        }
         
        AnimalGranja[] farmAnimals = new AnimalGranja[2];
        farmAnimals[0] = new Vaca();
        farmAnimals[1] = new Gallina();
        
        for(AnimalGranja a : farmAnimals) {
            System.out.println(a.produceLeche());
        }
        
        Perro dog1 = new Perro();
        Perro dog2 = new Perro();
        Perro dog3 = new Perro();
        
        dog1.setBreedType(1);
        dog2.setBreedType(2);
        dog3.setBreedType(1);
        
        System.out.println(dog1.equals("Otra cosa"));
        System.out.println(dog1.equals(dog2));
        System.out.println(dog1.equals(dog3));
    }  */
        Mascota fido = new Perro();
        Mascota chester = new Perro();
        
        chester.setColor("orange");
        fido.setColor("blue");
        
        Mascota kitty = new Gato();
        kitty.setColor("pink");
        
        System.out.println("Comparing fido and chester" + fido.equals(chester));
        System.out.println("Comparing fido and kitty" + fido.equals(kitty) );
    }
}
