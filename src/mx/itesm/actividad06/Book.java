/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad06;

/**
 *
 * @author luis_
 */

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qty;
    
    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        qty = 0;
    }
    
    public Book(String name, Author[] authors, double price, int qty) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qty = qty;
    }
    
    public String getName() {
        return name;
    }
    
    public Author[] getAuthors() {
        return authors;
    }
    
    public double getPrice() {
        return price;
    }
    
    public void setPrice(double price) {
        this.price=price;
    }
    
    public int getQty() {
        return qty;
    }
    
    public void setQty(int qty) {
        this.qty = qty;
    }
    
    public String getAuthorNames() {
        String names = "" ;
        for (Author element : authors) {
            names += element.getName() + " ,";
        }
        return names;
    }
    
    @Override
    public String toString() {
        String autores = "";
        for (Author element : authors){
            autores += element + ", ";
        }
        return "Book[name=" + name + ", authors = " + autores + "price=" + price + ", qty=" + qty + "]";
    }
}
