/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad04;

/**
 *
 * @author luis_
 */
public class PersonTest {
    public static void main(String[] args) {
        Person p = new Person("Luis", "Mexico");
        Student s = new Student("Alan", "Querétaro", "ITI", 3, 200.0);
        Staff st = new Staff("Daniela", "Puebla", "Tec", 500.0);
        System.out.println(p);
        System.out.println(s);
        System.out.println(st);
        //Pruebas Person
        System.out.println(p.getAddress() + p.getName());
        p.setAdress("Dubai");
        System.out.println(p);
        //Pruebas Student
        System.out.println(s.getAddress() + s.getName() + s.getProgram() + s.getYear() + s.getFee());
        s.setAdress("Tokyo");
        s.setFee(250.0);
        s.setProgram("IMI");
        s.setYear(1);
        System.out.println(s);
        //Pruebas Staff
        System.out.println(st.getAddress() + st.getName() + st.getSchool() + st.getPay());
        st.setAdress("Innsbruck");
        st.setPay(600.0);
        st.setSchool("Tec CEM");
        System.out.println(st);
    }
}
