/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.itesm.actividad02;
import java.lang.Math;

/**
 *
 * @author luis_
 */
public class Utilidades {
    private String actividad;
    private int tiempo, a, b, c;
    private double peso, celsius, sueldo, prestaciones, tope;
    
    public static double calorias(String actividad, int tiempo, double peso) {
        int met = 0;
        if (actividad.equalsIgnoreCase("correr")){
            met = 10;
        }
        else if (actividad.equalsIgnoreCase("basquetbol")){
            met = 9;
        }
        else if (actividad.equalsIgnoreCase("tenis")){
            met = 4;
        }
        else if (actividad.equalsIgnoreCase("bailar")){
            met = 3;
        }
        else if (actividad.equalsIgnoreCase("caminar")||actividad.equalsIgnoreCase("cocinar")){
            met = 2;
        }
        else if (actividad.equalsIgnoreCase("dormir")){
            met = 1;
        }
        double calorias = 0.0175 * met * peso * tiempo;
        return calorias;
    }
    
    public static double temperatura(double celsius) {
        return (9/5) * celsius + 32;
    }
    
    public static void cuadratica(int a, int b, int c) {
        double x1 =0;
        double x2 = 0;
        if (a*2 == 0) {
            System.out.println("El denominador no puede ser igual a cero");
        }
        else if (((Math.pow(b, 2)-(4*a*c)<0))) {
            System.out.println("La raíz no puede ser negativa");
        }
        else {
            double raiz = Math.pow(b, 2)-(4*a*c);
            x1 = (-b+(Math.sqrt(raiz))) / 2*a;
            x2 = (-b-(Math.sqrt(raiz))) / 2*a;
            String resultado = "Las soulciones de la cuadrática a: "+a+",b: "+b+",c: "+c+ " son: "+x1+" y "+x2;
            System.out.println(resultado);
        }
    }
    
    public static double progresion(int a, int b) {
        int n = b-a+1;
        double sucesion = ((a+b)*n)/2;
        return sucesion;
    }
    
    public static double sueldoFinal(double sueldo, double prestaciones, double tope) {
        double ingresoAcumulado = sueldo + prestaciones;
        double sueldoFinal = 0;
        if (ingresoAcumulado >= tope) {
            sueldoFinal = ingresoAcumulado * 0.65;
            return sueldoFinal;
        } else if (ingresoAcumulado < tope) {
            sueldoFinal = ingresoAcumulado * 0.8;
        }
        if (prestaciones < (sueldo/2)) {
            sueldoFinal += (sueldoFinal*0.5); 
        }
        return sueldoFinal;
    }
    
    
}
