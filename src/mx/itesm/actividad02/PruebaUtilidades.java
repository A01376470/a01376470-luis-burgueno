/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad02;
import java.util.Scanner;
/**
 *
 * @author luis_
 */
public class PruebaUtilidades {
    public static void main(String[] args) {
        
        while (true) {
            System.out.println( "***********" );
            System.out.println( "   Menu    " );
            System.out.println( "***********" );
            System.out.println( "1. Calorías Consumidas" );
            System.out.println( "2. Centigrados a Fahrenheit" );
            System.out.println( "3. Ecuacion de Segundo Grado" );
            System.out.println( "4. Progresión Aritmética" );
            System.out.println( "5. Sueldo Final" );
            System.out.println( "6. Salir" );
            System.out.println( "" );
            System.out.println( "" );
            System.out.print( "Dame tu opción: " );
            Scanner input = new Scanner(System.in);
            int opcion = input.nextInt();
            Utilidades u = new Utilidades();

            if (opcion == 1) {
                System.out.print( "Ingresa la actividad: " );
                String actividad = input.next();
                System.out.print( "Ingresa la duración de la actividad (en minutos enteros): " );
                int tiempo = input.nextInt();
                System.out.print("Ingresa el peso de la persona que va a hacer la actividad (en kilogramos con punto decimal): ");
                double peso = input.nextDouble();
                double resultado = u.calorias(actividad, tiempo, peso);
                if (resultado == 0) {
                    System.out.println("No se pudo completar la operación por un dato inválido");
                } else {
                    System.out.println("El total de calorías consumidas es: " + resultado);
                }
            }
            
            if (opcion == 2) {
                System.out.print("Ingresa la temperatura en grados centígrados: ");
                double celsius = input.nextDouble();
                double resultado = u.temperatura(celsius);
                System.out.println(celsius + " grados centígrados, equivalen a: " + resultado);
                
            }
            
            if (opcion == 3) {
                System.out.print("Ingresa el valor de a: ");
                int a = input.nextInt();
                System.out.print("Ingresa el valor de b: ");
                int b = input.nextInt();
                System.out.print("Ingresa el valor de c: ");
                int c = input.nextInt();
                u.cuadratica(a, b, c);  
            }
            
            if (opcion == 4) {
                System.out.print("Ingresa el valor de a: ");
                int a = input.nextInt();
                System.out.print("Ingresa el valor de b: ");
                int b = input.nextInt();
                double resultado = u.progresion(a, b);
                System.out.println("El resultado de la progresión es: " + resultado);
            }
            if (opcion == 5) {
                System.out.print("Ingresa el sueldo: ");
                double sueldo = input.nextDouble();
                System.out.print("Ingresa el valor de las prestaciones: ");
                double prestaciones = input.nextDouble();
                System.out.print("Ingresa el valor del tope: ");
                double tope = input.nextDouble();
                double ingresoFinal = u.sueldoFinal(sueldo, prestaciones, tope);
                System.out.println("El sueldo final es: " + ingresoFinal);
            }
            
            if (opcion == 6){
                System.out.println("¡Vuelva pronto!");
                break;
            }
        }
    }
}
