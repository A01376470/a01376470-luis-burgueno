/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad09;

/**
 *
 * @author luis_
 */
public interface Pet {
    String getName();
    void changeName(String name);
    String play();
}
