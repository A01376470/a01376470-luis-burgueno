/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad09;

/**
 *
 * @author luis_
 */
public abstract class Animal {
    protected int legs;
    public Animal(int legs){
        this.legs = legs;
    }
    public String walk(){
        return "I am walking with my " + legs + " legs";
    }
    public abstract String eat();
}
