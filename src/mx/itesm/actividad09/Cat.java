/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad09;

/**
 *
 * @author luis_
 */
public class Cat extends Animal implements Pet {
    private String name;
    public Cat(){
        super(4);
    }
    public Cat(String name) {
        super(4);
        this.name=name;
    }
    public String getName(){
        return name;
    }
     public void changeName(String name){
         this.name = name;
     }
     
     public String play(){
         return "I don't like to play very much, bust I love chasing fast moving objects";
     }
     
     public String eat() {
         return "I eat meat and drink milk";
     }
     
}
