/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad09;

/**
 *
 * @author luis_
 */
public class Fish extends Animal implements Pet{
    private String name;
    public Fish(){
        super(0);
    }
    public String getName(){
        return name;
    }
     public void changeName(String name){
         this.name = name;
     }
     public String play(){
         return "Look at me, I do not want to play with you";
     }
     public String eat(){
         return "I eat pelleted food";
     }
     public String walk(){
         return "I can swim, but I can't walk";
     }
}
