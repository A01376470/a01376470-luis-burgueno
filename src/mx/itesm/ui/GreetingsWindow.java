/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.ui;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

/**
 *
 * @author luis_
 */
public class GreetingsWindow extends JFrame implements ActionListener{
    
    private JLabel text;
    private JTextField inputField;
    private JButton button;
    
    public GreetingsWindow(){
    super();
    initializeWindowsState();
    initializeComponents();
    
    }
    
    private void initializeWindowsState(){
        setTitle("Greetings");
        setBounds(150, 250, 300, 300);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    private void initializeComponents(){
        text = new JLabel();
        text.setText("Type your name: ");
        text.setBounds(50, 50, 100, 25);
        getContentPane().add(text);
        
        inputField = new JTextField();
        inputField.setBounds(150, 50, 100, 30);
        getContentPane().add(inputField);
        
        button = new JButton();
        button.setText("Show greetings");
        button.setBounds(50, 100, 200, 30);
        button.addActionListener(this);
        getContentPane().add(button);
        
                
    }
    
    public void actionPerformed(ActionEvent e) {
        String name = inputField.getText();
        JOptionPane.showMessageDialog(getContentPane(), "Hello, " + name + ".");
    }
    
}
